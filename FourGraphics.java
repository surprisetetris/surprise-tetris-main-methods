package tetris;

import java.awt.Point;

import javafx.scene.paint.Color;

public class FourGraphics extends MyGraphics{

	
	public FourGraphics() {
		this.color = Color.CHOCOLATE;
	}
	@Override
	public Point[] getPoints() {
		width = 2;
		height = 2;
		return new Point[] {
				new Point(position.x, position.y),
				new Point(position.x+1, position.y+1),
				new Point(position.x, position.y+1),
				new Point(position.x+1, position.y)
		};
	}

	@Override
	public void changeDirection() {
	}


}

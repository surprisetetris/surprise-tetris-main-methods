package tetris;

import java.awt.Point;

import javafx.scene.paint.Color;

public class ZGraphics extends MyGraphics{
	private int direction = 0;

	public ZGraphics() {
		this.color = Color.SKYBLUE;
	}
	@Override
	public Point[] getPoints() {
		if(direction==0) {
			width = 3;
			height = 2;
			return new Point[] {
					new Point(position.x, position.y),
					new Point(position.x+1, position.y),
					new Point(position.x+1, position.y+1),
					new Point(position.x+2, position.y+1)
			};
		}else {
			width = 2;
			height = 3;
			return new Point[] {
					new Point(position.x+1, position.y),
					new Point(position.x+1, position.y+1),
					new Point(position.x, position.y+1),
					new Point(position.x, position.y+2)
			};
		}
	}

	@Override
	public void changeDirection() {
		
		direction = 1-direction;
	}
	
}

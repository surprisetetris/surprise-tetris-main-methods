package tetris;

import java.awt.Point;

import javafx.scene.paint.Color;

public class LineGraphics extends MyGraphics{
	private int direction = 0;

	public LineGraphics() {
		this.color = Color.KHAKI;
	}
	@Override
	public Point[] getPoints() {
		if(direction==0) {
			width = 4;
			height = 1;
			return new Point[] {
					new Point(position.x, position.y),
					new Point(position.x+1, position.y),
					new Point(position.x+2, position.y),
					new Point(position.x+3, position.y)
			};
		}else {
			width = 1;
			height = 4;
			Point points[] = new Point[4];
			for(int i = 0; i<4; i++) {
				points[i] = new Point(position.x,position.y+i);
			}return points;
		}
	}

	@Override
	public void changeDirection() {
		
		direction = 1-direction;
	}
	
}

package tetris;

import java.awt.Point;

import javafx.scene.paint.Color;

public class SlashGraphics extends MyGraphics{
	private int direction = 0;

	public SlashGraphics() {
		this.color = Color.LIGHTSLATEGRAY;
	}
	@Override
	public Point[] getPoints() {
		if(direction==0) {
			width = 2;
			height = 2;
			return new Point[] {
					new Point(position.x, position.y+1),
					new Point(position.x+1, position.y),
			};
		}else {
			width = 2;
			height = 2;
			return new Point[] {
					new Point(position.x,position.y),
					new Point(position.x+1,position.y+1),
			};
		}
	}

	@Override
	public void changeDirection() {
		
		direction = 1-direction;
	}
	
}


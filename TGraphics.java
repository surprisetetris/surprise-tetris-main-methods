package tetris;

import java.awt.Point;

import javafx.scene.paint.Color;

public class TGraphics extends MyGraphics{

	private int direction = 0;
	
	public TGraphics() {
		this.color = Color.GOLDENROD;
	}
	@Override
	public Point[] getPoints() {
		switch(direction) {
		case 0:width = 3;height = 2;return new Point[] {
				new Point(position.x, position.y),
				new Point(position.x+1, position.y),
				new Point(position.x+2, position.y),
				new Point(position.x+1, position.y+1)
		};
		case 1:width = 2;height = 3;return new Point[] {
				new Point(position.x+1, position.y),
				new Point(position.x, position.y+1),
				new Point(position.x+1, position.y+1),
				new Point(position.x+1, position.y+2)
		};
		case 2:width = 3;height = 2;return new Point[] {
				new Point(position.x+1, position.y),
				new Point(position.x, position.y+1),
				new Point(position.x+1, position.y+1),
				new Point(position.x+2, position.y+1)
		};
		default:width = 2;height = 3;return new Point[] {
				new Point(position.x, position.y),
				new Point(position.x, position.y+1),
				new Point(position.x+1, position.y+1),
				new Point(position.x, position.y+2)
		};
		}
	}

	@Override
	public void changeDirection() {
		direction = (direction + 1)%4;
	}


}

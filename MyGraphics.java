package tetris;

import java.awt.Point;
import javafx.scene.paint.Color;

public abstract class MyGraphics implements Cloneable{
	protected Point position;
	protected Color color;
	protected int width;
	protected int height;
	public abstract Point[] getPoints();
	boolean reverse = false;
	
	@Override  
    public Object clone() {  
        MyGraphics stu = null;  
        try{  
            stu = (MyGraphics)super.clone();  
        }catch(CloneNotSupportedException e) {  
            e.printStackTrace();  
        }  
        return stu;  
    }

	
	public abstract void changeDirection();

	//relative position
	public void setPosition(int i, int j) {
		position = new Point(i,j);
		
	}

	public Color getColor() {
		return color;
	}	
	
	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public Point getPosition() {
		return position;
	}

	/*
	 * 改变位置，向左（-1，0），向右（1，0），加速（0，1）
	 */
	public void changePosition(int x, int y) {
		position.x += x;
		position.y += y;
	}
}

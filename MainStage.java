package tetris;

import java.awt.Point;
import java.io.File;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.util.Duration;

public class MainStage extends Application {

	// Private Instance Variables

	// The current going graphics
	MyGraphics mygraphics;

	// The next graphics that will appear
	MyGraphics mygraphicsNext;

	// The left up point of the game stage
	Point left_up = new Point(40, 40);

	// A special graphics that will appear randomly
	// if the special exists, then special_set_on = true
	MyGraphics mygraphicsSpecial;
	boolean special_set_on = false;

	// the rows and columns of the game stage
	// default value is 30,25
	int rows = 25;
	int cols = 30;

	// The width and height of the game stage
	// will be calculated later on
	int width;
	int height;

	// Each square is 31.5px in length
	// The actual block has a 2px gap

	// The score of the current player
	int score = 0;

	// the speed of the falling block, default 700
	int frequency = 700;

	/*
	 * Array represents the stage if a[1][1] = 1, the block is occupied if a[1][1] =
	 * 0, the block is empty
	 */
	int values[][];
	// color stores the color of each occupied blocks
	Color color[][];

	// The pane with tetris
	Pane pane = new Pane();

	// Timeline is used to repeat fresh()
	Timeline animation;

	// The input from the users, represent the number of cols and rows
	ChoiceBox<Integer> cb_rows = new ChoiceBox<Integer>(
			FXCollections.observableArrayList(10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25));

	ChoiceBox<Integer> cb_cols = new ChoiceBox<Integer>(
			FXCollections.observableArrayList(15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30));

	ChoiceBox<Double> cb_fqc = new ChoiceBox<Double>(
			FXCollections.observableArrayList(0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0));

	// scene and stages

	// scene with the real game
	Scene scene;

	// scene with the starting panel
	Scene startScene;

	// scene with guided panel
	Scene guideScene;

	// stage for setting
	Stage rc;

	// main stage
	Stage mainP = new Stage();

	// The address of the customized image
	String default_img = "";
	// if customized image is chosen, the customize_img = true
	boolean customize_img = false;

	// img is the image chosen
	Image img;

	// The width and height of the screen
	int screenWidth = (int) Screen.getPrimary().getVisualBounds().getWidth();
	int screenHeight = (int) Screen.getPrimary().getVisualBounds().getHeight();

	// The gap between each block
	double gap = 2;

	// tf shows the address of the chosen image
	TextField tf = new TextField();

	// if the game is over, playnow=false
	boolean playnow = true;

	// Three buttons in the welcome panel
	Button start, setting, guide;

	// The background image of the program
	Image bcg = new Image("file:\\C:\\Users\\my computer\\Desktop\\try.jpg");
	ImageView imgvbcg = new ImageView(bcg);

	// Read the logo
	Image logoI = new Image("file:\\C:\\Users\\my computer\\Desktop\\logo.png");
	ImageView logo = new ImageView(logoI);

	/*
	 * Main method required
	 */

	public static void main(String[] args) {
		launch(args);

	}

	/*
	 * This method draws the welcome panel, provided with three buttons Start: call
	 * the begin method Setting: call the settingAction method Guide: call the guide
	 * method
	 */
	@Override
	public void start(Stage arg0) throws Exception {

		// This method shows the first stage, the welcome page with three buttons:
		// start, setting, and guide

		// Button start: start the game with the given settings
		start = new Button("START");
		start.setOnAction((EventHandler<ActionEvent>) new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				begin();
			}
		});

		// Button setting: call the setting panel
		setting = new Button("SETTING");
		setting.setOnAction((EventHandler<ActionEvent>) new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				settingAction();
			}
		});

		// Button guide: will call the guide method
		guide = new Button("GUIDE");
		guide.setOnAction((EventHandler<ActionEvent>) new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				guide();
			}
		});

		// set the width and the height of the main Stage
		mainP.setWidth(screenWidth * 18 / 25);
		mainP.setHeight(screenHeight * 7 / 8);

		// set the width and height of the background image
		imgvbcg.setFitHeight(mainP.getHeight());
		imgvbcg.setFitWidth(mainP.getWidth());

		prepaint();
	
		//show the main stage and make it centered
		mainP.centerOnScreen();
		mainP.show();
	}

	/*
	 * This method will paint the welcome panel with a rectangle and three buttons and a background
	 */
	public void prepaint() {

		// tempPane is the pane with the welcome panel
		Pane tempPane = new Pane();

		int mainWidth = (int) mainP.getWidth();
		int mainHeight = (int) mainP.getHeight();

		// determine the height and width of the playground

		// Two options: 7/10 of the screen width is the width of the playground
		// Or 4/5 of the height of the screen is the height of the playground
		if (mainWidth * 7 / 10 / cols * rows > mainHeight) {
			height = mainHeight * 4 / 5;
			width = height / rows * cols;
		} else {

			width = (int) (mainWidth * 7 / 10);
			height = (int) (width / cols * rows);
		}

		// draw the playground
		Rectangle bg = new Rectangle(left_up.x, left_up.y, width, height);
		bg.setFill(new Color(1, 1, 1, 0.4));

		// Set the locations of the buttons
		start.setLayoutX(left_up.x + width + 50);
		setting.setLayoutX(left_up.x + width + 50);
		guide.setLayoutX(left_up.x + width + 50);
		start.setLayoutY(left_up.y + height / 2 - 100);
		setting.setLayoutY(left_up.y + height / 2);
		guide.setLayoutY(left_up.x + height / 2 + 100);

		// Set the text color white
		start.setTextFill(Color.WHITE);
		setting.setTextFill(Color.WHITE);
		guide.setTextFill(Color.WHITE);

		// Add the element in
		tempPane.getChildren().addAll(imgvbcg, start, setting, guide, bg);

		// set the property of the scene
		startScene = new Scene(tempPane);

		// Use the log.css file to customize the button
		startScene.getStylesheets().add(MainStage.class.getResource("log.css").toExternalForm());

		// Set the property of the stage
		mainP.setScene(startScene);
	}

	/*
	 * This method shows the guide of the game
	 */
	public void guide() {





		//Set the text that will be displayed in the guide area
		TextArea des = new TextArea("\n\n Full fill a line to clear that line and get score! \n\n"
				+ "Tap on left or right to make the block go to the left or right \n\n"
				+ "Tap on down to let the block move down faster \n\n"
				+ "Tap on up to change the direction of the block \n\n" + "\n Keep in mind! \n\n"
				+ "20% of the chance, the block will refuse to move down after you tap UP \n\n"
				+ "20% of the chance, the block will reverse its motion \n direction and say goodbye to you after you tap UP \n"
				+ "Good Luck!"
				+ "\n Please Visit the Wiki Page of the program for detailed user guide:\n"
				+ "http://www.baidu.com");

		des.setStyle("-fx-font-size: 15pt");

		//This button will exit the current scene
		Button btExit = new Button("Exit");
		VBox guidePane = new VBox(des, btExit);
		btExit.setOnAction((EventHandler<ActionEvent>) new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				try {
					//try to redraw the welcome page
					start(new Stage());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				}
		});
		
		//draw the guide pane 
		Scene guideScene = new Scene(guidePane, mainP.getWidth(), mainP.getHeight());
		
		//add it in
		mainP.setScene(guideScene);

	}

	private void settingAction() {

		// set the default value of the choice boxes
		cb_cols.setValue(30);
		cb_rows.setValue(25);
		cb_fqc.setValue(0.7);

		// This button will save the changes
		Button submit = new Button("Save");
		// set the style of the button
		submit.setStyle("-fx-border-color: red;");

		// after being clicked, submitHandler will be called
		submit.setOnAction(new submitHandler());
		FlowPane pane_rc = new FlowPane();

		// Labels, to help identify the property of each choice box
		Label r = new Label("How many rows?"); //to ask about rows
		Label c = new Label("How many cols?"); //to ask about columns
		Label f = new Label("How fast?"); //to ask about speed
		Button butOpen = new Button("Customize your block Image"); //to set the image
		tf.setEditable(false); //do not let the user change the path directly
		tf.setText("Default Color"); //if no pictures, say "Default color"

		butOpen.setOnAction((EventHandler<ActionEvent>) new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				chooser();
			}
		});

		// add the labels in the pane and make it centered
		pane_rc.getChildren().addAll(r, cb_rows, c, cb_cols, f, cb_fqc, tf, butOpen, submit);
		pane_rc.setAlignment(Pos.CENTER);
		pane_rc.setHgap(10);

		Scene scene_rc = new Scene(pane_rc, 500, 100);
		//show the setting stage
		rc = new Stage();
		rc.setScene(scene_rc);
		rc.show();

	}

	/*
	 * This method help to select a photo
	 * This code is learned from CSDN: https://blog.csdn.net/weixin_42080534/article/details/82966320
	 */
	protected void chooser() {
		//set the file chooser
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Choose an image");
		fileChooser.setInitialDirectory(new File("."));
		File result = fileChooser.showOpenDialog(rc);

		// if there is a photo, then read it
		if (result != null) {
			//store the path of the image
			default_img = "file:\\" + result.getAbsolutePath();
			//read the image
			img = new Image(default_img);
			//set to true
			customize_img = true;
			
			tf.setText(result.getAbsolutePath());
		}
	}

	/*
	 * This method starts the game
	 */
	public void begin() {

		int mainWidth = (int) mainP.getWidth();
		int mainHeight = (int) mainP.getHeight();

		// The same as setting the size of the play ground in start()
		if (mainWidth * 4 / 5 / cols * rows > mainHeight) {
			height = mainHeight * 4 / 5;
			width = height / rows * cols;
		} else {

			width = (int) (mainP.getWidth() * 7 / 10);
			height = (int) (width / cols * rows);
		}

		// initialize the two array
		values = new int[rows][cols];
		color = new Color[rows][cols];

		// The time line will follow the procedure
		EventHandler<ActionEvent> eventHandler = e -> {

			// if the game is not ended, fresh the program
			if (playnow)
				fresh();
			// if not end, re draw the panel
			if (playnow)
				draw();

			// if the game ends, stop the game, and show the score of the player
			if (!playnow) {
				animation.stop();
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.titleProperty().set("Game is over!");
				alert.headerTextProperty().set("Your score is " + score);
				alert.show();
			}

		};

		// set the frequency and bind the eventHandler
		animation = new Timeline(new KeyFrame(Duration.millis(frequency), eventHandler));
		animation.setCycleCount(Timeline.INDEFINITE);

		// scene is the play scene
		scene = new Scene(pane);
		mainP.setScene(scene);
		mainP.setTitle("Special Tetris: Playing!");
		mainP.centerOnScreen();
		generate();
		draw();

		scene.setOnKeyPressed(new EventHandler<KeyEvent>() {

			/*
			 * This method is the event handler that process the information from the
			 * keyboard UP, DOWN, LEFT, RIGHT can be processed
			 */
			@Override
			public void handle(KeyEvent event) {

				// if UP is pressed
				if (event.getCode() == KeyCode.UP) {

					// if there is a special block, then turn the two blocks
					if (special_set_on) {
						if (canTurn()) {
							mygraphics.changeDirection();
							mygraphicsSpecial.changeDirection();
						}
						draw();
						return;
					}

					// if there is only a single block

					// if the blocks is moving upward, then let it move upward
					if (mygraphics.reverse && canMoveUp()) {
						mygraphics.changePosition(0, -1);
						return;
					}

					// generate a random number to determine the probability
					double random = Math.random();

					// for 20% chance, the block that has a y>4 will reverse its motion
					if (random > 0.80 && mygraphics.position.y > 4) {
						mygraphics.reverse = true;
					}

					// for another 20% chance, the block will stop its motion
					if (random < 0.2 && mygraphics.position.y > 3 && !mygraphics.reverse) {
						// the block reaches the end
						addGraphics(mygraphics);
						deleteLine(); // see if any lines can be deleted
						// generate a new pair of graphics
						generate();
					}

					// the rest of the probability is to turn the block
					if (canTurn() && !mygraphics.reverse) {
						mygraphics.changeDirection();
					}

					draw();

				}
				if (event.getCode() == KeyCode.DOWN) {

					// if there is a special block,
					if (special_set_on) {
						// if both can move down, then both move down
						if (canMoveDown() && SpecialCanMoveDown()) {
							mygraphics.changePosition(0, 1);
							mygraphicsSpecial.changePosition(0, 1);
							draw();
							return;

						} else if (canMoveDown()) {
							// if only one can move down, let it move down
							mygraphics.changePosition(0, 1);
							draw();
							return;
						} else if (SpecialCanMoveDown()) {

							mygraphicsSpecial.changePosition(0, 1);
							draw();
							return;
						} else {
							return;
						}
					}

					// if it can move down, move down
					if (canMoveDown())
						mygraphics.changePosition(0, 1);
					draw();

				}
				if (event.getCode() == KeyCode.LEFT) {

					// if two blocks can both move left, move left
					if (canChangeLeft() && special_set_on) {
						mygraphics.changePosition(-1, 0);
						mygraphicsSpecial.changePosition(-1, 0);
						draw();
						return;
					} else if (special_set_on) {
						return;
					}

					// if can move left, move left
					if (canChangeLeft())
						mygraphics.changePosition(-1, 0);
					draw();
				}
				if (event.getCode() == KeyCode.RIGHT) {
					// both can move right, move right
					if (canChangeRight() && special_set_on) {
						mygraphics.changePosition(1, 0);
						mygraphicsSpecial.changePosition(1, 0);
						draw();
						return;
					} else if (special_set_on) {
						return;
					}
					// if can move right, move right
					if (canChangeRight())
						mygraphics.changePosition(1, 0);
					draw();
				}
			}
		});

		mainP.show();
		animation.play();
	}

	class submitHandler implements EventHandler<ActionEvent> {

		@Override
		public void handle(ActionEvent arg0) {

			// set the values if there is a value
			if (cb_rows.getValue() != null)
				rows = cb_rows.getValue();
			if (cb_cols.getValue() != null)
				cols = cb_cols.getValue();
			if (cb_fqc.getValue() != null)
				frequency = (int) (1000 * cb_fqc.getValue());// convert to mili second

			//close the current setting stage
			rc.close();
			prepaint();
		}
	}

	/*
	 * This method will add all graphics to the pane Can be used to repaint the
	 * panel
	 */
	public void draw() {

		// clear the existing elements
		pane.getChildren().clear();
		// add the background pic
		pane.getChildren().add(imgvbcg);

		// draw the playground
		Rectangle bg = new Rectangle(left_up.x, left_up.y, width, height);
		bg.setFill(new Color(1, 1, 1, 0.85));
		pane.getChildren().add(bg);

		{
			// draw the moving block
			if (mygraphics != null) {
				// get the points
				Point points[] = mygraphics.getPoints();
				for (Point p : points) {

					if (customize_img) {
						// if the image is customized, then fill with the image
						ImageView imgv = new ImageView(img);
						imgv.setFitHeight(height / rows);
						imgv.setFitWidth(width / cols);
						imgv.setX(left_up.x + (p.x * width / cols) + gap);
						imgv.setY(left_up.y + ((p.y) * height) / rows + gap);
						pane.getChildren().add(imgv);
					} else {
						// else, draw the rectangles
						Rectangle r = new Rectangle(left_up.x + (p.x * width / cols) + gap,
								left_up.y + ((p.y) * height) / rows + gap, width / cols - 2 * gap,
								height / rows - 2 * gap);
						r.setFill(mygraphics.getColor());
						pane.getChildren().add(r);
					}

				}
			}
		}

		// draw the special block if there is one
		if (special_set_on) {
			// add the moving graphics to the pane
			Point points[] = mygraphicsSpecial.getPoints();
			for (Point p : points) {

				if (customize_img) {
					ImageView imgv = new ImageView(img);
					imgv.setFitHeight(height / rows);
					imgv.setFitWidth(width / cols);
					imgv.setX(left_up.x + (p.x * width / cols) + gap);
					imgv.setY(left_up.y + ((p.y) * height) / rows + gap);
					pane.getChildren().add(imgv);
				} else {
					Rectangle r = new Rectangle(left_up.x + (p.x * width / cols) + gap,
							left_up.y + ((p.y) * height) / rows + gap, width / cols - 2 * gap, height / rows - 2 * gap);
					r.setFill(mygraphics.getColor());
					pane.getChildren().add(r);
				}

			}
		}

		// add all static graphics to the pane
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				if (values[i][j] == 1) {
					if (customize_img) {
						ImageView imgv = new ImageView(img);
						imgv.setFitHeight(height / rows);
						imgv.setFitWidth(width / cols);
						imgv.setX(left_up.x + (j * width) / cols + gap);
						imgv.setY(left_up.y + (i * height) / rows + gap);
						pane.getChildren().add(imgv);
					} else {
						Rectangle r = new Rectangle(left_up.x + (j * width) / cols + gap,
								left_up.y + (i * height) / rows + gap, width / cols - 2 * gap, height / rows - 2 * gap);
						r.setFill(color[i][j]);
						pane.getChildren().add(r);
					}
				}
			}

		}

		// draw the next block
		if (mygraphicsNext != null) {
			// add the next graphics to the pane
			Point nextPoint[] = mygraphicsNext.getPoints();
			for (Point p : nextPoint) {
				Rectangle r = new Rectangle(
						left_up.x + width + 50 + ((p.x - mygraphicsNext.getPosition().x) * width / cols) + gap,
						left_up.y + cols / 2 + ((p.y - mygraphicsNext.getPosition().y) * height) / rows + gap,
						width / cols - 2 * gap, height / rows - 2 * gap);
				r.setFill(mygraphicsNext.getColor());
				pane.getChildren().add(r);
			}
		}
	}

	/*
	 * This method generate graphics to mygraphics and mygraphicsNext Can be used to
	 * initialize a graph
	 */
	public void generate() {

		// if there is no block, generate it
		if (mygraphicsNext == null) {
			mygraphicsNext = createGraphics();
		}

		// change the "next" to "now", and generate a new "next"
		mygraphics = mygraphicsNext;
		mygraphicsNext = createGraphics();

		// there is 10% chance that a special block will be generated
		if (Math.random() < 0.1) {
			String s = mygraphics.getClass().getName();
			// generate the same existing block
			switch (s) {
			case "tetris.LineGraphics":
				mygraphicsSpecial = new LineGraphics();
				break;
			case "tetris.TGraphics":
				mygraphicsSpecial = new TGraphics();
				break;
			case "tetris.FourGraphics":
				mygraphicsSpecial = new FourGraphics();
				break;
			case "tetris.ZGraphics":
				mygraphicsSpecial = new ZGraphics();
				break;
			case "tetris.AntiZGraphics":
				mygraphicsSpecial = new AntiZGraphics();
				break;
			case "tetris.LGraphics":
				mygraphicsSpecial = new LGraphics();
				break;
			case "tetris.SlashGraphics":
				mygraphicsSpecial = new SlashGraphics();
				break;
			case "tetris.UGraphics":
				mygraphicsSpecial = new UGraphics();
				break;
			case "tetris.BridgeGraphics":
				mygraphicsSpecial = new BridgeGraphics();
				break;
			}

			int pos = mygraphics.getPosition().x;
			// there will be 5-7 gap between the blocks
			// % is used to make sure that the block will not be out of bounds
			pos = (pos + 5 + (int) Math.random() * 3) % (cols - mygraphics.getWidth() - 1);
			mygraphicsSpecial.setPosition(pos, 0);

			special_set_on = true;
		}

	}

	/*
	 * This method generate a graphic and return it
	 * 
	 * @return a new graphics (block)
	 */
	public MyGraphics createGraphics() {
		MyGraphics temp = null;
		// assign a random graphics to temp
		int n = (int) (Math.random() * 9);
		switch (n) {
		case 0:
			temp = new LineGraphics();
			break;
		case 1:
			temp = new TGraphics();
			break;
		case 2:
			temp = new FourGraphics();
			break;
		case 3:
			temp = new ZGraphics();
			break;
		case 4:
			temp = new AntiZGraphics();
			break;
		case 5:
			temp = new LGraphics();
			break;
		case 6:
			temp = new SlashGraphics();
			break;
		case 7:
			temp = new UGraphics();
			break;
		case 8:
			temp = new BridgeGraphics();
			break;
		}

		// The graphic will appear at a random location
		n = (int) (Math.random() * (cols - 5));
		temp.setPosition(n, 0);
		return temp;
	}

	/*
	 * This method determines whether the block can turn or not if there is a
	 * special block, then it will determine both blocks
	 * 
	 * @return true if both block or a single block can turn
	 * 
	 * @return false if one of them cannot
	 */
	public boolean canTurn() {

		// whether this turn will go over the boundary
		boolean a = mygraphics.getHeight() + mygraphics.getPosition().x <= cols;
		boolean b = mygraphics.getWidth() + mygraphics.getPosition().y <= rows;

		// whether this turn will overlap with other blocks
		MyGraphics temp = (MyGraphics) mygraphics.clone();
		temp.changeDirection();
		Point[] temp_point = temp.getPoints();
		for (Point p : temp_point) {
			if (p.y >= 0 && p.x >= 0 && p.y < rows && p.x < cols && values[p.y][p.x] == 1)
				return false;
		}

		boolean c = true;
		boolean d = true;

		// if there is a special block
		if (special_set_on) {
			c = mygraphicsSpecial.getHeight() + mygraphicsSpecial.getPosition().x <= cols;
			d = mygraphicsSpecial.getWidth() + mygraphicsSpecial.getPosition().y <= rows;

			// whether this turn will overlap with other blocks
			MyGraphics temp_s = (MyGraphics) mygraphicsSpecial.clone();
			temp.changeDirection();
			Point[] temp_s_point = temp_s.getPoints();
			for (Point p : temp_s_point) {
				if (p.y >= 0 && p.x >= 0 && p.y < rows && p.x < cols && values[p.y][p.x] == 1)
					return false;
			}

		}

		return a && b && c && d;
	}

	/*
	 * determine whether the block can move down or not
	 * 
	 * @return true if can move down, false otherwise
	 */
	public boolean canMoveDown() {
		Point points[] = this.mygraphics.getPoints();
		boolean find = false;
		// in case that the block is reversed, avoid OutOfBounds exception
		if (mygraphics.getPosition().y < 0)
			return true;

		// find all the places below the block
		for (Point p : points) {
			if (p.y == rows - 1 || values[p.y + 1][p.x] == 1) {
				find = true;
				break;
			}
		}

		return !find;
	}

	/*
	 * determine whether the special block can move down or not
	 * 
	 * @return true if can move down, false otherwise
	 */
	public boolean SpecialCanMoveDown() {
		Point points[] = this.mygraphicsSpecial.getPoints();
		boolean find = false;
		// special block will not move up, so only determine whether there is a block
		// below
		for (Point p : points) {
			if (p.y == rows - 1 || values[p.y + 1][p.x] == 1) {
				find = true;
				break;
			}
		}
		return !find;
	}

	/*
	 * This method determines the next step for the game, whether to continue
	 * moving, or generate a new graphic
	 */
	public void fresh() {

		// if there is a special block,
		if (special_set_on) {
			// if both can move down, move down
			if (canMoveDown() && SpecialCanMoveDown()) {
				this.draw();
				mygraphics.changePosition(0, 1);
				mygraphicsSpecial.changePosition(0, 1);
				return;
			} else if (canMoveDown()) {
				// the special block reaches the end, since it cannot move
				addGraphics(mygraphicsSpecial);
				deleteLine(); // see if any lines can be deleted

				// set it to false
				special_set_on = false;

				mygraphicsSpecial = null;
				// move the block down
				mygraphics.changePosition(0, 1);
				return;
			} else if (SpecialCanMoveDown()) {
				// the normal block reaches the end

				addGraphics(mygraphics);

				deleteLine(); // see if any lines can be deleted

				// assign the special to normal
				mygraphics = mygraphicsSpecial;
				// make it empty
				mygraphicsSpecial = null;
				// set this to false
				special_set_on = false;
				// move one down
				mygraphics.changePosition(0, 1);
				return;
			}

			else {
				{
					// both blocks reach the end

					addGraphics(mygraphics);

					addGraphics(mygraphicsSpecial);

					deleteLine(); // see if any lines can be deleted
					// generate a new pair of graphics
					this.draw();
					special_set_on = false;
					mygraphicsSpecial = null;
					generate();
					return;
				}
			}

		}

		// if there is only one block
		if (!mygraphics.reverse && canMoveDown()) {
			// not reversed
			// the block will move down
			this.mygraphics.changePosition(0, 1);

		} else if (!mygraphics.reverse) {
			// cannot move down
			if (mygraphics.getPosition().y == 0) {
				// stops at the top
				// game ends
				playnow = false;
				return;
			}

			// the block reaches the end
			addGraphics(mygraphics);
			deleteLine(); // see if any lines can be deleted
			// generate a new pair of graphics
			generate();

		} else if (canMoveUp()) {
			// if reversed, then move up
			mygraphics.changePosition(0, -1);
		} else if (mygraphics.getPosition().y > 0) {
			// the block reaches the end
			addGraphics(mygraphics);
			deleteLine(); // see if any lines can be deleted
			// generate a new pair of graphics
			this.draw();
			generate();

		} else {
			this.draw();
			generate();

		}

	}

	/*
	 * Traverse the panel to see if any line can be deleted
	 */
	public void deleteLine() {
		for (int i = 0; i < rows; i++) {
			int count = 0;
			for (int j = 0; j < cols; j++) {
				if (values[i][j] == 1) {
					count++;
				} else {
					break;
				}
			}
			// If the current line is full of blocks, delete it
			if (count == cols) {

				for (int k = i; k > 0; k--) {
					for (int m = 0; m < cols; m++) {
						values[k][m] = values[k - 1][m];
						color[k][m] = color[k - 1][m];
					}
				}
				// add 10 to the score
				score += 10;
			}
		}
	}

	/*
	 * This method determines whether the block can move to the right or not
	 * 
	 * @return true if it can move right, false otherwise
	 */
	public boolean canChangeRight() {

		boolean find = false;
		{
			Point points[] = this.mygraphics.getPoints();

			for (Point p : points) {
				if (p.x == cols - 1 || values[p.y][p.x + 1] == 1) {
					find = true;
					break;
				}

			}
		}

		// find out if the special can move right or not
		boolean addFind = false;

		if (special_set_on) {
			Point points[] = this.mygraphicsSpecial.getPoints();

			for (Point p : points) {
				if (p.x == cols - 1 || values[p.y][p.x + 1] == 1) {
					addFind = true;
					break;
				}

			}
		}

		return !find && !addFind;
	}

	/*
	 * This method determines whether the block can move left or not
	 * 
	 * @return true if can move left
	 * 
	 * @return false otherwise
	 */
	public boolean canChangeLeft() {
		boolean find = false;
		{
			Point points[] = this.mygraphics.getPoints();

			for (Point p : points) {
				if (p.x == 0 || values[p.y][p.x - 1] == 1) {
					find = true;
					break;
				}

			}
		}
		boolean addFind = false;

		if (special_set_on) {
			Point points[] = this.mygraphicsSpecial.getPoints();

			for (Point p : points) {
				if (p.x == 0 || values[p.y][p.x - 1] == 1) {
					addFind = true;
					break;
				}

			}
		}

		return !find && !addFind;
	}

	/*
	 * This method determines whether the block can move up or not
	 * 
	 * @return true if can move up, false other wise
	 * 
	 * @precondition: mygraphics.reversed = true
	 */
	public boolean canMoveUp() {
		Point points[] = this.mygraphics.getPoints();
		boolean find = false;
		if (mygraphics.getPosition().y > 0) {

			for (Point p : points) {
				if (values[p.y - 1][p.x] == 1) {
					find = true;
					break;
				}

			}
			return !find;
		} else if (mygraphics.getPosition().y + mygraphics.height < 0) {
			return false;
		} else
			return true;
	}

	/*
	 * This method add a block to static
	 * 
	 * @param the block that needs to be added
	 * 
	 */
	public void addGraphics(MyGraphics graphics) {
		Point points[] = graphics.getPoints();
		Color color = graphics.getColor();
		for (Point p : points) {
			values[p.y][p.x] = 1;
			this.color[p.y][p.x] = color;
		}
	}

}

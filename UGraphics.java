package tetris;

import java.awt.Point;

import javafx.scene.paint.Color;

public class UGraphics extends MyGraphics{
	private int direction = 0;

	public UGraphics() {
		this.color = Color.NAVY;
	}
	@Override
	public Point[] getPoints() {
		if(direction==0) {
			width = 3;
			height = 2;
			return new Point[] {
					new Point(position.x, position.y),
					new Point(position.x, position.y+1),
					new Point(position.x+1, position.y+1),
					new Point(position.x+2, position.y+1),
					new Point(position.x+2, position.y),
			};
		}else if(direction==1){
			width = 2;
			height = 3;
			return new Point[] {
					new Point(position.x,position.y),
					new Point(position.x+1,position.y),
					new Point(position.x,position.y+1),
					new Point(position.x,position.y+2),
					new Point(position.x+1,position.y+2),
			};
		}else if(direction==2) {
			width = 3;
			height = 2;
			return new Point[] {
					new Point(position.x, position.y),
					new Point(position.x, position.y+1),
					new Point(position.x+1, position.y),
					new Point(position.x+2, position.y),
					new Point(position.x+2, position.y+1),
			};
		}
		else {
			width = 2;
			height = 3;
			return new Point[] {
					new Point(position.x,position.y),
					new Point(position.x+1,position.y+1),
					new Point(position.x+1,position.y+2),
					new Point(position.x+1,position.y),
					new Point(position.x,position.y+2),
			};
		}
	}

	@Override
	public void changeDirection() {
		direction = (direction + 1)%4;
	}
	
}

